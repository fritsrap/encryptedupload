/*
Bsp:

showModal({
		titleHTML: "title",
		bodyHTML: "bodyHTML",
		okBtnText: "Ok",
		saveBtnText: "Speichern",
		onSaveClick: function(mod) { console.log("save"); },
		onOkClick: function(mod) { console.log("onOkClick"); mod.modal('hide'); },
		onHidden: function() { console.log("HIDDDDDDEN"); },
		beforeRender: function(mod) { console.log("can change the dom obj 'mod' if I want!"); }
	});

*/

function showModal(newOptions) {
	var options = {
		titleHTML: "title",
		bodyHTML: "bodyHTML",
		okBtnText: "Ok",
		saveBtnText: "Speichern",
		onSaveClick: null,
		onOkClick: null,
		onHidden: null,
		beforeRender: null,
		afterRender: null,
		draggable: null,
		large: null,
		backdrop: null,
		modalstyle: null
	}
	for (var i in newOptions) {
		options[i] = newOptions[i];
	}

	var lg = '';
	if (options['large'])
		lg = 'modal-lg';
	var customModalStyle = 'style="' + options["modalstyle"] + '"' || "";

	var backdrop = options["backdrop"] ? 'data-backdrop="static"' : "";

	var mod = $(`
	<div ${backdrop} class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div ${customModalStyle} class="modal-dialog" ${lg} role="document">
			<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title myModalLabel" id="exampleModalLabel">Modal title</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				...
			</div>
			<div class="modal-footer">
				<button type="button" class="modalOkBtn btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="modalSaveBtn btn btn-primary">Save changes</button>
			</div>
			</div>
		</div>
	</div>
	`);

	$("body").append(mod);
	mod.find(".myModalLabel").html(options["titleHTML"]);
	if(options["bodyIsJqElement"]) {
		console.log("!")
		mod.find(".modal-body").empty().append(options["bodyHTML"]);
	} else {
		mod.find(".modal-body").html(options["bodyHTML"]);
	}	 

	if (options["okBtnText"]) {
		if (options["okBtnText"] == "hide") {
			mod.find('.modalOkBtn').hide();
		} else {
			mod.find('.modalOkBtn').text(options["okBtnText"]);
		}
	}

	if (options["saveBtnText"]) {
		if (options["saveBtnText"] == "hide") {
			mod.find('.modalSaveBtn').hide();
		} else {
			mod.find('.modalSaveBtn').text(options["saveBtnText"]);
		}
	}

	mod.on('hidden.bs.modal', function (e) {
		if (options["onHidden"]) {
			options["onHidden"]();
		}
		mod.remove();
		if ($("body .modal-dialog").length == 0) { //Check if a modal is still open
			$(".modal-backdrop").remove();
			$("body").css({ "padding-right": "0px" });
		}
	});

	mod.find(".modalOkBtn").click(function (evt) {
		if (options["onOkClick"]) {
			options["onOkClick"](mod);
		} else {
			mod.modal('hide');
		}
	});

	mod.find(".modalSaveBtn").click(function (evt) {
		if (options["onSaveClick"]) {
			options["onSaveClick"](mod);
		} else {
			mod.modal('hide');
		}
	});

	if (options["beforeRender"]) {
		options["beforeRender"](mod);
	}

	mod.modal('show');

	if (options["afterRender"]) {
		options["afterRender"](mod);
	}

	if (options["draggable"]) {
		mod.find(".modal-header").mousedown(draggable_modal);
		mod.find(".modal-header").css({ "cursor": "move" });
	}
}

/* BEISPIEL!
showErrorModal({
	titleHTML: "Fehler",
	bodyHTML: 'Fehlerbeschreibung'
}, false, "");
*/
function showErrorModal(newOptions, sendFunction, err) {
	showModal({
		titleHTML: newOptions["titleHTML"],
		bodyHTML: newOptions["bodyHTML"],
		okBtnText: newOptions["okBtnText"],
		onHidden: newOptions["onHidden"],
		beforeRender: function (mod) {
			mod.find(".modal-header").css("background", "#ff5f5f");
			if (!sendFunction) {
				mod.find(".modalSaveBtn").hide();
			}
		},
		saveBtnText: "Fehler senden!",
		onSaveClick: function (mod) {
			mod.modal('hide');
			note("Fehler gesendet!");
			var sendString = 'Beschreibung: ' + mod.find(".beschreibung").val() + '\n\r\n\r' + err;
			socket.emit("sendError", sendString);
		}
	});
}

function draggable_modal(e) {
	window.my_dragging = {};
	my_dragging.pageX0 = e.pageX;
	my_dragging.pageY0 = e.pageY;
	my_dragging.elem = $($(this).parents(".modal-dialog")[0]);
	my_dragging.offset0 = $(this).offset();
	function handle_dragging(e) {
		var left = my_dragging.offset0.left + (e.pageX - my_dragging.pageX0);
		var top = my_dragging.offset0.top + (e.pageY - my_dragging.pageY0);
		$(my_dragging.elem)
			.offset({ top: top, left: left });
	}
	function handle_mouseup(e) {
		$('body')
			.off('mousemove', handle_dragging)
			.off('mouseup', handle_mouseup);
	}
	$('body')
		.on('mouseup', handle_mouseup)
		.on('mousemove', handle_dragging);
}