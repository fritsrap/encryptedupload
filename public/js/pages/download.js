var downloadPageLoaded = false;
async function loaddownloadPage() {
    if (downloadPageLoaded) {
        return;
    }
    downloadPageLoaded = true;

    document.getElementById('privKeyUpload').addEventListener('change', readPrivKeyFile, false);

    function readPrivKeyFile(evt) {
        var files = evt.target.files;
        var file = files[0];
        console.log(file, file.type)

        var reader = new FileReader();
        reader.onload = async function (event) {
            console.log("Try loading privkey in!");
            try {
                var fileContent = ab2str(event.target.result);
                var privKeyObj = JSON.parse(fileContent);

                privKey = await crypto.subtle.importKey(
                    "jwk",
                    privKeyObj,
                    { name: "RSA-OAEP", hash: "SHA-512" },
                    false,
                    ["decrypt"]
                );

                loadFileListIn();

                var keyMatchStr = '<i title="Sie verwenden Privaten Schlüssel der nicht zum momentan hinterlegten öffentlichen Schlüssel passt. Dateien können ggf. nicht entschlüsselt werden!" class="fas fa-exclamation-triangle"></i>';
                if (pubkeyObj.n != privKeyObj.n) {
                    alert("Der private Schlüssel stimmt nicht mit dem aktuellen öffentlichen überein! Dateien können möglicherweiße nicht gelesen werden!")
                } else {
                    keyMatchStr = '<i class="fas fa-check"></i>';
                }

                $("#privKeyUploadContaier").hide();
                $("#privkeyStart").html("<i>Privater Schlüssel hinterlegt! Passender öffentlicher Schlüssel: " + privKeyObj.n.substr(0, 30) + "...</i> " + keyMatchStr);


            } catch (e) {
                console.log(e)
                alert("Given File is not a valid PrivKey!");
            }
        }
        reader.readAsArrayBuffer(file)
    }

    function loadFileListIn() {
        sendPostReq("/getAllFiles", {}, function (data) {
            $("#fileTableContainer").empty();
            var fileTable = $(`<table class="table display" style="width:100%">
                <thead>
                    <tr>
                        <th>Dateiname</th>
                        <th>Uploader</th>
                        <th></th>
                        <th><input class="massdownloadGlobalCheck" type="checkbox"></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>
                            <button style="position: relative; left: -15px;" type="button" class="downloadSelected btn btn-link"><i class="fas fa-download"></i></button>
                            <button style="position: relative; left: -15px;" type="button" class="deleteSelected btn btn-link"><i class="fas fa-trash-alt"></i></button>
                        </th>
                    </tr>
                </tfoot>
            </table>`);

            $("#fileTableContainer").append(fileTable);
            fileTable.find(".massdownloadGlobalCheck").click(function () {
                if ($(this).is(":checked")) {
                    $.each($(".massdownloadCheck"), function () {
                        if (!$(this).is(":checked")) {
                            $(this).click();
                        }
                    })
                } else {
                    $.each($(".massdownloadCheck"), function () {
                        if ($(this).is(":checked")) {
                            $(this).click();
                        }
                    })
                }
            })
            fileTable.find(".downloadSelected").click(function () {
                $.each($(".massdownloadCheck"), function () {
                    if ($(this).is(":checked")) {
                        $(this).parents("tr").find(".dlBtn").click();
                    }
                })
            })

            fileTable.find(".deleteSelected").click(function () {
                var deleteArray = [];
                $.each($(".massdownloadCheck"), function () {
                    if ($(this).is(":checked")) {
                        deleteArray.push($(this).parents("tr").find(".filename").attr("filename"))
                    }
                })
                console.log(deleteArray)
                if(deleteArray.length>0) {
                    showModal({
                        titleHTML: "Dateien löschen",
                        bodyHTML: `Wollen Sie wirklich ${deleteArray.length} Datei(en) wirklich vom Server löschen?<br>
                            Löschpasswort:<input class="form-control deletepassword" type="password" placeholder="password">
                        `,
                        okBtnText: "Abbruch",
                        saveBtnText: "Löschen",
                        onSaveClick: function(mod) { 
                            sendPostReq("/removeFiles", { filenames : deleteArray, password : mod.find(".deletepassword").val() }, function(data) {
                                if (data.error) {
                                    alert(data.error);
                                    return;
                                }
                                alert("Datei(en) erfolgreich gelöscht!");
                                loadFileListIn();
                            })
                            mod.modal('hide');
                         },
                        onOkClick: function(mod) { mod.modal('hide'); },
                    });
                } else {
                    alert("Wählen Sie mindestens eine Datei aus!")
                }
            })

            for (var i in data) {
                (function () {
                    var filename = data[i];
                    var tr = $(`
                    <tr>
                        <td class="filename" filename="${filename}">${filename}</td>
                        <td>${filename.split("_")[0]}</td>
                        <td><button type="button" class="dlBtn btn btn-link"><i class="fas fa-download"></i> Herunterladen</button></td>
                        <td><input class="massdownloadCheck" type="checkbox"></td>
                    </tr>
                    `)
                    tr.find("button").click(function () {
                        var _this = this;
                        $(this).hide();
                        console.log("download", filename)

                        //First load the AES KEY File and decrypt it
                        var oReq = new XMLHttpRequest();
                        oReq.open("GET", subdir + "/uploads/" + filename + ".aesKey", true);
                        oReq.responseType = "arraybuffer";

                        oReq.onload = async function (oEvent) {
                            console.log(oEvent)
                            var arrayBuffer = oReq.response; // Note: not oReq.responseText
                            //Decrypt AES Key with given RSA key
                            await crypto.subtle.decrypt({ name: "RSA-OAEP", hash: "SHA-256" }, privKey, arrayBuffer).catch(function () {
                                alert("Privater Schlüssel passt nicht zu dieser Datei! Verwenden Sie einen anderen Privaten Schlüssel!");
                                $(_this).show();
                            }).then(async function (dec) {
                                var aesKeyObj = JSON.parse(ab2str(dec));
                                var iv = base64StringToArray(aesKeyObj.iv);

                                //Import the given AES Key
                                const aesKey = await crypto.subtle.importKey("jwk", aesKeyObj, { name: "AES-CBC", length: 256 }, false, ["encrypt", "decrypt"]);

                                //Now load the actual file
                                var oReq1 = new XMLHttpRequest();
                                oReq1.open("GET", subdir + "/uploads/" + filename, true);
                                oReq1.responseType = "arraybuffer";

                                oReq1.onload = async function (oEvent) {
                                    console.log(oEvent)
                                    var arrayBuffer = oReq1.response; // Note: not oReq1.responseText
                                    const dec = await crypto.subtle.decrypt({ name: "AES-CBC", iv: iv }, aesKey, arrayBuffer);
                                    console.log(dec)
                                    $(_this).show();
                                    var blob = new Blob([dec]);
                                    var objectUrl = URL.createObjectURL(blob);
                                    const a = document.createElement('a');
                                    a.href = objectUrl;
                                    a.download = filename || 'download';
                                    a.click();
                                    //window.open(objectUrl);
                                }

                                oReq1.error = function (event) {
                                    console.log(event)
                                }
                                oReq1.send(null);

                            });
                        }

                        oReq.error = function (event) {
                            console.log(event);
                            alert("Download error!")
                        }
                        oReq.send(null);

                    })

                    fileTable.find("tbody").append(tr);
                })()
            }

            fileTable.DataTable({
                "language": {
                    "url": "./js/DataTables_de.json"
                },
                "columnDefs": [{
                    "targets": [3],
                    "orderable": false
                }]
            });

            var refreshBtn = $('<button style="width: 240px; float: right;" class="form-control" id="refreshDataList"><i class="fas fa-sync"></i> Dateiliste aktuallisieren!</button>');
            $("#fileTableContainer").append(refreshBtn)
            refreshBtn.click(function() {
                loadFileListIn();
            })
            console.log(data)
        })
    }
}