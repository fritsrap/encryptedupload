var activePage = null;
var pubKey;

var url = document.URL.substr(0, document.URL.lastIndexOf('/'));
var urlSplit = url.split("/");
var subdir = "";
for (var i = 3; i < urlSplit.length; i++) {
  subdir = subdir + '/' + urlSplit[i];
}

var userHash = localStorage.getItem("userHash");
var userName = localStorage.getItem("userName");

$(document).ready(async function () {
  'use strict'

  $(document).on("keydown", function(event) {
    if(event.which == 13 && $(".modalSaveBtn").is(":visible")) {
      event.preventDefault();
      $(".modalSaveBtn").click();
    }
  })

  var logoutTimeOut = null;
  $(document).on("click", function(event) {
    if(logoutTimeOut) {
      clearTimeout(logoutTimeOut);
    }

    logoutTimeOut = setTimeout(function() {
      $("#logoutLink").click();
      alert("Sie wurden wegen inaktivität automatisch abgemeldet!")
    }, 1000*60*20); //20 min
  })

  const startPage = "#uploadPage";
  function showLoginModal() {
    var loginTable = $(`<table style="width: 100%;">
      <tr>
        <td>Benutzer:</td>
        <td><input class="userInput form-control" type="text"></td>
      </tr>
      <tr>
        <td>Passwort:</td>
        <td><input class="passwortInput form-control" type="password"></td>
      </tr>
    </table>`)

    showModal({
      titleHTML: "Login",
      bodyHTML: loginTable,
      okBtnText: "hide",
      saveBtnText: "login",
      backdrop: true,
      onSaveClick: function (mod) {
        var username = mod.find(".userInput").val().trim();
        var password = mod.find(".passwortInput").val().trim();
        sendPostReq("/login", { user: username, password: password }, function (content) {
          if (content.err) {
            showErrorModal({
              titleHTML: "Fehler",
              bodyHTML: content.err
            }, false, "");
          } else {
            console.log(content)
            userHash = content.data.userHash;
            userName = content.data.uid;
            localStorage.setItem("userHash", userHash);
            localStorage.setItem("userName", userName);
            $("#username").text(userName)
            mod.modal('hide');
            $(".nav,#logoutBtnWrapper").show();
            loadPage("uploadPage")

          }
        })
      },
      beforeRender: function (mod) { mod.find(".modal-header").find("button").hide() }
    });
  }
  if (!userHash) {
    showLoginModal()
  } else {
    sendPostReq("/checkHash", { userHash: userHash }, function (content) {
      if (content.error) {
        showLoginModal();
      } else {
        $("#username").text(userName);
        $(".nav,#logoutBtnWrapper").show();
      }
    })
  }

  $(".page").hide();

  var urlLocation = location.toString().split("#");
  if (urlLocation.length > 0 && urlLocation[1] != "" && $("#" + urlLocation[1]).length == 1) {
    $("#" + urlLocation[1]).show()
    $(".nav-link.active").removeClass("active");
    $.each($(".nav-link"), function () {
      if ($(this).attr("href") == "#" + urlLocation[1]) {
        $(this).addClass("active")
      }
    });
    loadPage("#" + urlLocation[1]);
  } else {
    $(startPage).show();
    loadPage(startPage);
  }

  $(".nav-link").click(function () {
    var herf = $(this).attr("href");
    $(".page").hide();
    $(herf).show();
    $(".nav-link.active").removeClass("active");
    $(this).addClass("active");
    loadPage(herf);

  })

  function loadPage(pageId) {
    if ($("#sidebarMenu:visible").length && $(".navbar-toggler:visible").length) {
      $(".navbar-toggler:visible").click()
    }

    activePage = pageId;

    if (pageId == "#uploadPage") {
      loaduploadPage();
    } if (pageId == "#fulluploadedPage") {
      fulluploadedPage();
    } if (pageId == "#downloadPage") {
      loaddownloadPage();
    } else if (pageId == "#keyPage") {
      loadkeyPage();
    } else if (pageId == "#logout") {
      localStorage.removeItem("userHash");
      localStorage.removeItem("userName");
      $(".nav,#logoutBtnWrapper").hide();
    }
  }

  $('[data-toggle="tooltip"]').tooltip();

  await crypto.subtle.importKey(
    "jwk",
    pubkeyObj,
    { name: "RSA-OAEP", hash: "SHA-512" },
    false,
    ["encrypt"]
  ).then(function (key) {
    pubKey = key;
    console.log("PublicKey imported!", key)
  }).catch(function (e) {
    console.error("Could not import publickey!", e);
    alert("Could not import publickey!");
  });
});

//HELPER FUNCTIONS! -------------------

function sendPostReq(url, data, callback) {
  data["userHash"] = userHash;
  $.ajax({
    type: "POST",
    url: subdir + '' + url,
    // The key needs to match your method's input parameter (case-sensitive).
    data: JSON.stringify(data),
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    success: callback,
    failure: function (errMsg) {
      alert(errMsg);
    }
  });
}

function formatDate(date, withClock, short) {
  if (date == "0000-00-00" || date == "0000-00-00 00:00:00" || date == "" || date == "-" || date == "NaN-NaN-NaN" || date == null || !(new Date(date))) {
    return "-";
  }
  var d = new Date(date);
  var day = d.getDate();
  if (day < 10) {
    //@ts-ignore
    day = '0' + day;
  }
  var month = (d.getMonth() + 1);
  if (month < 10) {
    //@ts-ignore
    month = '0' + month;
  }
  if (withClock) {
    return day + '.' + month + '.' + d.getUTCFullYear() + ' ' + d.getHours() + ':' + d.getMinutes();
  }
  if (short) {
    return day + '.' + month + '.' + (d.getUTCFullYear() + '').replace("20", "");
  }
  if (!day && day != 0) {
    return date;
  }
  return day + '.' + month + '.' + d.getUTCFullYear();
}

function formatDateForInput(date) {
  var d = new Date(date);
  var month = (d.getMonth() + 1);
  if (month < 10) {
    //@ts-ignore
    month = "0" + month;
  }

  var day = (d.getDate());
  if (day < 10) {
    //@ts-ignore
    day = "0" + day;
  }

  return (d.getUTCFullYear() + "-" + month + '-' + day);
}

//Gets string and returns the arraybuffer
function str2ab(str) {
  var buf = new ArrayBuffer(str.length * 2); // 2 bytes for each char
  var bufView = new Uint16Array(buf);
  for (var i = 0, strLen = str.length; i < strLen; i++) {
    bufView[i] = str.charCodeAt(i);
  }
  return buf;
}

//Gets buffer and returns an String
function ab2str(buf) {
  return String.fromCharCode.apply(null, new Uint16Array(buf));
}

//Gernerates an random Arraybuffer
function getRandomBytes(length) {
  var crypto = (self.crypto || self.msCrypto), QUOTA = 65536;
  var a = new Uint8Array(length);
  for (var i = 0; i < length; i += QUOTA) {
    crypto.getRandomValues(a.subarray(i, i + Math.min(length - i, QUOTA)));
  }
  return a;
}

function arrayToBase64String(a) {
  return btoa(String.fromCharCode(...a));
}

function base64StringToArray(s) {
  let asciiString = atob(s);
  return new Uint8Array([...asciiString].map(char => char.charCodeAt(0)));
}