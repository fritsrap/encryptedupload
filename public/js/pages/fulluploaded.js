async function fulluploadedPage() {
    sendPostReq("/getAllFiles", {}, function (data) {
        var ownFile = false;

        var ownUploadedFileTable = $(`
            <table class="table display" style="width:100%" id="ownUploadedFileTable">
            <thead>
            <tr>
                <th>Nr.</th>
                <th>Dateiname</th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
        `)

        $("#ownUploadedFileTableContainer").empty();
        var nr = 1;
        for (var i in data) {
            if (data[i].startsWith(userName + '_')) {
                ownFile = true;
                ownUploadedFileTable.find("tbody").append(`<tr><td>${nr++}</td><td>${data[i]}</td></tr>`)
            }
        }
        if (!ownFile) {
            $("#ownUploadedFileTableContainer").html(`Sie haben noch keine Datei hochgeladen!`)
        } else {
            $("#ownUploadedFileTableContainer").append(ownUploadedFileTable)
            ownUploadedFileTable.DataTable({
                "language": {
                    "url": "./js/DataTables_de.json"
                }
            });
            $("#ownUploadedFileTableContainer").append('<div><br><br><i>Info: Dateien können ersetzt werden indem Sie mit exakt dem gleichen Namen erneut hochgeladen werden!</i></div>')
        }
        console.log(data)
    })

}