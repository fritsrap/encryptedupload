var keyPageLoaded = false;
async function loadkeyPage() {
    if (keyPageLoaded) {
        return;
    }
    keyPageLoaded = true;
    console.log("LOAD KEYPAGE")

    $("#generateNewKeyPairBtn").click(async function () {
        var _this = this;
        $(this).hide();
        $("#generateNewKeyFeedback").html('<i class="fas fa-spinner fa-pulse"></i> Bitte warten...');
        var generateNewKeyPairPassword = $("#generateNewKeyPairPassword").val().trim();
        let keyPair = await window.crypto.subtle.generateKey(
            {
                name: "RSA-OAEP",
                modulusLength: 4096,
                publicExponent: new Uint8Array([1, 0, 1]),
                hash: "SHA-512"
            },
            true,
            ["encrypt", "decrypt"]
        );
        var publicKeyObj = await crypto.subtle.exportKey("jwk", keyPair.publicKey);
        var privateKeyObj = await crypto.subtle.exportKey("jwk", keyPair.privateKey);
        sendPostReq("/changeKeyPair", {
            password: generateNewKeyPairPassword,
            publicKeyObj: publicKeyObj
        }, function (data) {
            $(_this).show();
            if (data.error) {
                alert(data.error);
                $("#generateNewKeyFeedback").empty()
                return;
            }
            var dec = str2ab(JSON.stringify(privateKeyObj, null, 2));
            var blob = new Blob([dec], { type: 'application/json' });
            var objectUrl = URL.createObjectURL(blob);
            const a = document.createElement('a');
            a.href = objectUrl;
            a.download = "privKey.rsa" || 'download';
            a.click();
            $("#generateNewKeyFeedback").html('<br><br><i class="fas fa-check"></i> Neues Schlüsselpaar generiert und hinterlegt!<br><b>Achtung</b>: Ohne den neuen Privaten Schlüssel können hochgeladene Dateien nicht entschlüsselt werden.<br>Die Seite muss <b>neu geladen</b> werden um die Änderungen zu übernehmen!')
            console.log(data)
        })
    })

    $("#deleteAllFilesBtn").click(async function () {
        var _this = this;
        $(this).hide();
        var deleteAllFilesPassword = $("#deleteAllFilesPassword").val().trim();
        console.log(deleteAllFilesPassword)
        sendPostReq("/removeAllFiles", {
            password: deleteAllFilesPassword
        }, function (data) {
            $(_this).show();
            if (data.error) {
                alert(data.error);
                $("#generateNewKeyFeedback").empty()
                return;
            } else {
                $("#deleteAllFilesFeedback").html('Alle Dateien wurden entfernt!');
                $("#fileTableContainer").empty();
                $("#privKeyUploadContaier").show();
            }

        });
    });

}