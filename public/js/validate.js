function validate(ident, succCallback, errCallback) {
	var err = false;
	$(ident).find("input.notEmpty, select.notEmpty, input.mustNumber, input.mustInt, input.mustDate, input.mustDateAfter2010, input.mustIban, input.mustBic, input.mustEmail").each(function () {
		$(this).val($.trim($(this).val()));
		var localError = false;
		var localTitle = "";
		if ($(this).hasClass("notEmpty")) {
			if ($(this).val() == "" || $(this).val() == null) {
				localError = true;
				localTitle = "Dieses Feld darf nicht leer sein!";
				$(this).attr("title", localTitle);
			}
		}

		if (!localError && $(this).hasClass("mustInt")) {
			if (!isInt($(this).val())) {
				localError = true;
				localTitle = "Dieses Feld muss eine Ganzahl enthalten!";
				$(this).attr("title", localTitle);
			}
		}

		if (!localError && $(this).hasClass("mustNumber")) {
			if (!isNumber($(this).val())) {
				localError = true;
				localTitle = "Dieses Feld muss eine Zahl enthalten!";
				$(this).attr("title", localTitle);
			}
		}

		if (!localError && $(this).hasClass("biggerThan")) {
			if (!$(this).attr("biggerThan") || parseFloat($(this).val()) <= parseFloat($(this).attr("biggerThan"))) {
				localError = true;
				localTitle = "Die Zahl muss größer als " + $(this).attr("biggerThan") + " sein!";
				$(this).attr("title", localTitle);
			}
		}

		if (!localError && $(this).hasClass("biggerEqualThan")) {
			if (!$(this).attr("biggerEqualThan") || parseFloat($(this).val()) < parseFloat($(this).attr("biggerEqualThan"))) {
				localError = true;
				localTitle = "Die Zahl muss größer oder gleich " + $(this).attr("biggerEqualThan") + " sein!";
				$(this).attr("title", localTitle);
			}
		}

		if (!localError && $(this).hasClass("smallerThan")) {

			if (!$(this).attr("smallerThan") || parseFloat($(this).val()) >= parseFloat($(this).attr("smallerThan"))) {
				localError = true;
				localTitle = "Die Zahl muss kleiner als " + $(this).attr("smallerThan") + " sein!";
				$(this).attr("title", localTitle);
			}
		}

		if (!localError && $(this).hasClass("smallerEqualThan")) {

			if (!$(this).attr("smallerEqualThan") || parseFloat($(this).val()) > parseFloat($(this).attr("smallerEqualThan"))) {
				localError = true;
				localTitle = "Die Zahl muss kleiner oder gleich " + $(this).attr("smallerEqualThan") + " sein!";
				$(this).attr("title", localTitle);
			}
		}

		if (!localError && $(this).hasClass("mustDate")) {
			var date = $(this).val();
			var timestamp = Date.parse(date);
			if (isNaN(timestamp) == true) {
				localError = true;
				localTitle = "Dieses Feld muss ein gültiges Datum enthalten!";
				$(this).attr("title", localTitle);
			}
		}

		if (!localError && $(this).hasClass("mustDateAfter2010")) {
			var date = $(this).val();
			var timestamp = Date.parse(date);
			if (isNaN(timestamp) == true || date.split("-")[0] < 2010) {
				localError = true;
				localTitle = "Dieses Feld muss ein gültiges Datum nach 2010 enthalten!";
				$(this).attr("title", localTitle);
			}
		}

		if (!localError && $(this).hasClass("minlength")) {
			if (!$(this).attr("minlength") || $(this).val().length < $(this).attr("minlength")) {
				localError = true;
				localTitle = "Die eingabe muss mindestens " + $(this).attr("minlength") + " Zeichen lang sein!";
				$(this).attr("title", localTitle);
			}
		}

		if (!localError && $(this).hasClass("mustIban")) {
			if (!isValidIBANNumber($(this).val())) {
				localError = true;
				localTitle = "Dieses Feld muss ein gültiges IBAN enthalten!";
				$(this).attr("title", localTitle);
			}
		}

		if (!localError && $(this).hasClass("mustBic")) {
			if (!isValidBICNumber($(this).val())) {
				localError = true;
				localTitle = "Dieses Feld muss ein gültiges BIC enthalten!";
				$(this).attr("title", localTitle);
			}
		}

		if (!localError && $(this).hasClass("mustEmail")) {
			if (!validateEmail($(this).val())) {
				localError = true;
				localTitle = "Dieses Feld muss ein gültiges E-mail enthalten!";
				$(this).attr("title", localTitle);
			}
		}

		if (localError && $(this).hasClass("canBeEmpty")) {
			if ($(this).val() == "") {
				localError = false;
			} else {
				localTitle = $(this).attr("title") + " Oder leer sein!";
				$(this).attr("title", localTitle);
			}
		}

		if (localError && $(this).is(":visible")) {
			err = true;
			$(this).addClass("redInputBorder");
			$(this).tooltip();

			if ($(this).is("select")) {
				var next = $(this).next();
				if (next.hasClass("select2")) {
					next.find(".select2-selection").addClass("redInputBorder");
					next.find(".select2-selection").attr("title", localTitle);
					next.find(".select2-selection").tooltip();
				}
			}
		} else {
			$(this).removeClass("redInputBorder");
			$(this).removeAttr("title");
			$(this).tooltip('hide');

			if ($(this).is("select")) {
				var next = $(this).next();
				if (next.hasClass("select2")) {
					next.find(".select2-selection").removeClass("redInputBorder");
					next.find(".select2-selection").removeAttr("title");
					next.find(".select2-selection").tooltip('hide');
				}
			}
		}
	});

	if (err)
		if (typeof (errCallback) != "undefined")
			errCallback();
		else
			console.log("Validate error but no callback function!");
	else
		succCallback();

}

function isInt(value) {
	return !isNaN(value) &&
		parseInt(Number(value)) == value &&
		!isNaN(parseInt(value, 10));
}

function isNumber(n) {
	return !isNaN(parseFloat(n)) && isFinite(n);
}

function isValidIBANNumber(input) {
	var CODE_LENGTHS = {
		AD: 24, AE: 23, AT: 20, AZ: 28, BA: 20, BE: 16, BG: 22, BH: 22, BR: 29,
		CH: 21, CR: 21, CY: 28, CZ: 24, DE: 22, DK: 18, DO: 28, EE: 20, ES: 24,
		FI: 18, FO: 18, FR: 27, GB: 22, GI: 23, GL: 18, GR: 27, GT: 28, HR: 21,
		HU: 28, IE: 22, IL: 23, IS: 26, IT: 27, JO: 30, KW: 30, KZ: 20, LB: 28,
		LI: 21, LT: 20, LU: 20, LV: 21, MC: 27, MD: 24, ME: 22, MK: 19, MR: 27,
		MT: 31, MU: 30, NL: 18, NO: 15, PK: 24, PL: 28, PS: 29, PT: 25, QA: 29,
		RO: 24, RS: 22, SA: 24, SE: 24, SI: 19, SK: 24, SM: 27, TN: 24, TR: 26
	};
	var iban = String(input).toUpperCase().replace(/[^A-Z0-9]/g, ''), // keep only alphanumeric characters
		code = iban.match(/^([A-Z]{2})(\d{2})([A-Z\d]+)$/), // match and capture (1) the country code, (2) the check digits, and (3) the rest
		digits;
	// check syntax and length
	if (!code || iban.length !== CODE_LENGTHS[code[1]]) {
		return false;
	}
	// rearrange country code and check digits, and convert chars to ints
	digits = (code[3] + code[1] + code[2]).replace(/[A-Z]/g, function (letter) {
		return letter.charCodeAt(0) - 55;
	});
	// final check
	return mod97(digits) === 1;
}

function mod97(string) {
	var checksum = string.slice(0, 2), fragment;
	for (var offset = 2; offset < string.length; offset += 7) {
		fragment = String(checksum) + string.substring(offset, offset + 7);
		checksum = parseInt(fragment, 10) % 97;
	}
	return checksum;
}

function isValidBICNumber(value) {
	var regSWIFT = /^([a-zA-Z]){4}([a-zA-Z]){2}([0-9a-zA-Z]){2}([0-9a-zA-Z]{3})?$/;
	return regSWIFT.test(value);
}

function validateEmail(email) {
	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
}