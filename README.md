# FH ENC

**Kurzfassung:** Dateiupload  der auf der Clientseite mit öffentlichem Schlüssel verschlüsselt. Dateien werden voll verschlüsselt übertragen und gespeichert. Entschlüsselt wird erst nach download, erneut auf der Clientseite, mit Privatem Schlüssel.


**Details**: RSA-OAEP (Asymmetrische verschlüsselung) kann in Kombination mit SHA-512 max. 382 Byte verschlüsseln -> siehe [Standard](https://tools.ietf.org/html/rfc8017#section-7.1.1). Daher ist hier ein [Hybrid Cryptosystem](https://en.wikipedia.org/wiki/Hybrid_cryptosystem) implementiert. 
Pro Datei wird hierbei ein AES Schlüssel (Symmetrische verschlüsselung) ezeugt welcher die Datei verschlüsselt. Anschließend wird dieser AES Schlüssel mit dem öffentlichen Schlüssel verschlüsselt und mit der Datei an den Server übertragen. Der AES Schlüssel und die Datei selbst können somit nur mit dem passenden Privaten Schlüssel entschlüsselt werden.

Schlüsselerzeugung:
https://developer.mozilla.org/en-US/docs/Web/API/SubtleCrypto/generateKey
```
RSA-OAEP: modulusLength = 4096; hash= SHA-512
AES-CBC: Schlüssellänge = 256bit
```



Verschlüsseln: https://developer.mozilla.org/en-US/docs/Web/API/SubtleCrypto/encrypt

Entschlüsseln: https://developer.mozilla.org/en-US/docs/Web/API/SubtleCrypto/decrypt

Schlüsselexport: https://developer.mozilla.org/en-US/docs/Web/API/SubtleCrypto/exportKey

## Create new keyChangePassword

Use the node function:
`crypto.createHash('sha256').update("myNewPassword").digest('base64');`

Put the new pw into the files "deleteAllFilesPasswordSha256.txt" and "keyChangePasswordSha256.txt" on the server!