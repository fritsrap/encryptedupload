const WEBPORT = 8080;
const uploadDir = './public/uploads';
var keyUpdatePassword;
var deleteAllFilesPassword;

var fs = require('fs')
const path = require('path');
var crypto = require('crypto');
const express = require('express');
const bodyParser = require("body-parser");
const fileUpload = require('express-fileupload');

var ldapRTApi = require('./ldapRT');
ldapRTApi.init("ldaps://dirserv.reutlingen-university.de:636");

var app = express();
app.use(express.static(__dirname + '/public'));
app.use(fileUpload());
var server = require('http').Server(app);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

if (!fs.existsSync(uploadDir)) {
    fs.mkdirSync(uploadDir);
}

fs.readFile('./keyChangePasswordSha256.txt', 'utf8', function (err, data) {
    if (err) throw err;
    keyUpdatePassword = data;
});

fs.readFile('./deleteAllFilesPasswordSha256.txt', 'utf8', function (err, data) {
    if (err) throw err;
    deleteAllFilesPassword = data;
});


app.post('/login', (req, res) => {
    var user_name = req.body.user;
    var password = req.body.password;
    //console.log("User name = " + user_name + ", password is " + password);

    ldapRTApi.isVaildUser(user_name, password, function (err, data, userHash) {
        if (err) {
            res.end(JSON.stringify({ err: "Falscher Benutzer oder Passwort!" }));
            return console.log("Login was wrong!", err)
        } else if (data && data.mail && data.mail.toLowerCase().endsWith("@reutlingen-university.de")) { //Check auf mitarbeiter
            ldapRTApi.getAllLdapUserGroups(user_name, password, false, function (err, groupdata) {
                data["groupdata"] = groupdata;
                data["userHash"] = userHash;
                res.end(JSON.stringify({ err: false, data: data }));
            })
        } else {
            res.end(JSON.stringify({ err: "Fehlende Berechtigungen!" }));
        }
    })
});

app.post('/checkHash', (req, res) => {
    var userHash = req.body.userHash;
    if (ldapRTApi.isActiveHash(userHash)) {
        res.end(JSON.stringify({ "error": false }));
    } else {
        res.end(JSON.stringify({ "error": "keine Rechte!" }));
    }
});

app.post('/getAllFiles', (req, res) => {
    var userHash = req.body.userHash;
    if (ldapRTApi.isActiveHash(userHash)) {
        var allFileNames = [];
        fs.readdirSync(uploadDir).forEach(file => {
            if (!file.endsWith(".aesKey")) {
                allFileNames.push(file)
            }
        });
        res.end(JSON.stringify(allFileNames));
    } else {
        res.end(JSON.stringify({ "error": "keine Rechte!" }));
    }
});

app.post('/newUpload', function (req, res) {
    var userHash = req.body.userHash;
    if (ldapRTApi.isActiveHash(userHash)) {
        if (!req.files || Object.keys(req.files).length === 0) {
            return res.status(400).send(JSON.stringify({ "error": 'No files were uploaded.' }));
        }

        //Basic form Upload, just store some files
        var uploadCnt = 0;

        for (var i in req.files) {
            uploadCnt++;
            (function () {
                var file = req.files[i];
                //console.log(file.name);
                file.name = file.name.replace(/[|&;$%@"<>()+,]/g, "");
                // Use the mv() method to place the file somewhere on your server
                file.mv(uploadDir + "/" + file.name, function (err) {
                    if (err)
                        return res.status(500).send(JSON.stringify({ "error": err }));

                    checkEnd()
                });
            })()
        }

        function checkEnd() {
            uploadCnt--;
            if (uploadCnt == 0) {
                res.send(JSON.stringify({ error: false, msg: 'Files uploaded!' }));
            }
        }
    } else {
        res.end(JSON.stringify({ "error": "keine Rechte!" }));
    }
});

app.post('/removeAllFiles', function (req, res) {
    var userHash = req.body.userHash;
    var password = req.body.password;
    if (ldapRTApi.isActiveHash(userHash)) {
        if (crypto.createHash('sha256').update(password).digest('base64') === deleteAllFilesPassword) {
            const directory = "./public/uploads";
            deleteDir(directory, function (err) {
                if (err) {
                    res.end(JSON.stringify({ "error": "Ups: Da lief was falsch!" }));
                } else {
                    res.end(JSON.stringify({ "error": false }));
                }
            })
        } else {
            res.end(JSON.stringify({ "error": "Falsches Passwort!" }));
        }
    } else {
        res.end(JSON.stringify({ "error": "keine Rechte!" }));
    }
});

app.post('/removeFiles', function (req, res) {
    var userHash = req.body.userHash;
    var password = req.body.password;
    var filenames = req.body.filenames || [];
    if (ldapRTApi.isActiveHash(userHash)) {
        if (crypto.createHash('sha256').update(password).digest('base64') === deleteAllFilesPassword) {
            var error = null;
            for (var i in filenames) {
                var filename = filenames[i];
                var path = "./public/uploads/" + filename;
                path = path.replace(/[|&;$%@"<>()+,]/g, "");
                try {
                    fs.unlinkSync(path);
                    fs.unlinkSync(path+'.aesKey');
                    
                } catch (err) {
                    error = err;
                }
            }
            if(error) {
                res.end(JSON.stringify({ "error": "Ups: Beim löschen der Daten lief was schief!" }));
            } else {
                res.end(JSON.stringify({ "error": false }));
            }
        } else {
            res.end(JSON.stringify({ "error": "Falsches Passwort!" }));
        }
    } else {
        res.end(JSON.stringify({ "error": "keine Rechte!" }));
    }
});

app.post('/changeKeyPair', function (req, res) {
    var userHash = req.body.userHash;
    var password = req.body.password;
    var publicKeyObj = req.body.publicKeyObj;

    if (ldapRTApi.isActiveHash(userHash)) {
        if (crypto.createHash('sha256').update(password).digest('base64') !== keyUpdatePassword) {
            res.end(JSON.stringify({ "error": "Falsches Passwort!" }));
            return;
        }
        try {
            if (publicKeyObj && publicKeyObj.alg == "RSA-OAEP-512" && publicKeyObj.kty == "RSA" && publicKeyObj.n && publicKeyObj.n.length > 32) {
                var pubKeyContent = 'const pubkeyObj =' + JSON.stringify(publicKeyObj, null, 2);
                fs.writeFile("./public/js/pubkey.rsa.js", pubKeyContent, function (err) {
                    if (err) {
                        console.log(err)
                        res.end(JSON.stringify({ "error": "Schlüsseldatei konnte nicht geschrieben werden!" }));
                        return;
                    };

                    const directory = "./public/uploads";
                    deleteDir(directory, function (err) {
                        if (err) {
                            res.end(JSON.stringify({ "error": "Ups: beim Datein löschen lief was schief!" }));
                        } else {
                            res.end(JSON.stringify({ "error": false }));
                        }
                    })
                });
            } else {
                res.end(JSON.stringify({ "error": "Übergebene Schlüsseldatei ist nicht korrekt!" }));
                return;
            }
        } catch (e) {
            console.log(e)
            res.end(JSON.stringify({ "error": "Da lief was schief! Schlüssel konnte nicht hinterlegt werden!" }));
        }
    } else {
        res.end(JSON.stringify({ "error": "keine Rechte!" }));
    }
});

var server = app.listen(WEBPORT, function () {
    var host = server.address().address
    var port = server.address().port
    console.log("App listening at http://%s:%s", host, port)
})

function deleteDir(directory, callback) {
    fs.readdir(directory, (err, files) => {
        if (err) {
            callback(err);
            return console.log(err);
        };

        for (const file of files) {
            fs.unlink(path.join(directory, file), err => {
                if (err) console.log(err);
            });
        }
        callback(false);
    });
}