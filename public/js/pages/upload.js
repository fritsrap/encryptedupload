var uploadPageLoaded = false;
var pubKey = null;
async function loaduploadPage() {
    if (uploadPageLoaded) {
        return;
    }
    uploadPageLoaded = true;

    $("#keyStart").html("<i/>Öffentlicher Schlüssel: " + pubkeyObj.n.substr(0, 30) + '...</i>');

    document.getElementById('fileUpload').addEventListener('change', readFile, false);

    function readFile(evt) {
        var files = evt.target.files;
        var fileUploadCnt = 0;
        var statusTable = $(`<table class="table" style="max-width: 900px;">
            <tr>
                <th>Dateiname</th>
                <th>Datei einlesen</th>
                <th>Dateischlüssel generieren</th>
                <th>Verschlüsseln</th>
                <th>Hochladen</th>
                <th>Abgeschlossen</th>
            </tr>
        </table>`);
        $("#uploadFeedback").empty().append(statusTable);
        for (var i = 0; i < files.length; i++) {
            (function () {
                var file = files[i];
                console.log(file, file.type)
                var tr = $(`<tr>
                    <td>${file.name}</td>
                    <td class="reading"><i class="fas fa-spinner fa-pulse"></i></td>
                    <td class="aesKey"><i class="fas fa-spinner fa-pulse"></i></td>
                    <td class="encrypt"><i class="fas fa-spinner fa-pulse"></i></td>
                    <td class="upload"><i class="fas fa-spinner fa-pulse"></i></td>
                    <td class="done"><i class="fas fa-spinner fa-pulse"></i></td>
                </tr>`)
                statusTable.append(tr);
                var reader = new FileReader();
                reader.onload = async function (event) {
                    tr.find('.reading').html('<i class="fas fa-check"></i>');
                    //We got the file and will encrypt it with an AES key. The key is encrypted with RSA and send to the server with the file.
                    //Details about the proccess: https://en.wikipedia.org/wiki/Hybrid_cryptosystem

                    console.log("File loaded: ", event.target.result, file.name);
                    //Generate AES key for encrypting the file
                    const aesKey = await crypto.subtle.generateKey({ name: "AES-CBC", length: 256 }, true, ["encrypt", "decrypt"]);
                    const iv = getRandomBytes(16); //generate Init aes vector

                    tr.find('.aesKey').html('<i class="fas fa-check"></i>');

                    //Encrypt file with AES
                    const encryptedFileData = await crypto.subtle.encrypt({ name: "AES-CBC", iv: iv }, aesKey, event.target.result).catch(function (err) { console.error(err) });

                    tr.find('.encrypt').html('<i class="fas fa-check"></i>');

                    //Export AES Key
                    var aesKeyExport = await crypto.subtle.exportKey("jwk", aesKey);
                    aesKeyExport.iv = arrayToBase64String(iv); //Save the init vector inside the key obj
                    var aesKeyExportAB = str2ab(JSON.stringify(aesKeyExport)); //Get AES Key as ArrayBuffer

                    //Encrypt AES Key with public RSA key
                    const ecnryptedAESKey = await crypto.subtle.encrypt({ name: "RSA-OAEP", hash: "SHA-256" }, pubKey, aesKeyExportAB).catch(function (err) { console.error(err) });
                    console.log("Encrypt ecnryptedAESKey: ", ecnryptedAESKey);

                    //Generate form Data to send
                    var data = new FormData();
                    //Add AES encrypted file to form
                    var encryptedFile = new File([encryptedFileData], userName + '_' + file.name, { type: file.type, lastModified: file.lastModified })
                    data.append('file-' + fileUploadCnt++, encryptedFile);
                    //Add RSA encrypted key to form
                    var ecnryptedAESKeyFile = new File([ecnryptedAESKey], userName + '_' + file.name + '.aesKey', { type: file.type, lastModified: file.lastModified })
                    data.append('file-' + fileUploadCnt++, ecnryptedAESKeyFile);
                    data.append('userHash', userHash);
                    $.ajax({
                        url: subdir + '/newUpload',
                        data: data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        method: 'POST',
                        type: 'POST', // For jQuery < 1.9
                        success: function (data) {
                            console.log(data)
                            data = JSON.parse(data)
                            if(data.error) {
                                alert(data.error);
                                return;
                            }
                            tr.find('.status').html("Upload done!");
                            tr.find('.upload').html('<i class="fas fa-check"></i>');
                            setTimeout(function () {
                                tr.find('.done').html('<i class="fas fa-check"></i>');
                            }, 500)
                        },
                        error: function (err) {
                            console.log(err);
                            tr.find('.status').html("Error while uploading!");
                        }
                    });

                }
                reader.readAsArrayBuffer(file)
            })();
        }

    }
}